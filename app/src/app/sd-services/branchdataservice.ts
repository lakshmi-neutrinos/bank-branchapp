/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
//CORE_REFERENCE_IMPORTS
import { SDBaseService } from '../../app/n-services/SDBaseService';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
//append_imports_start
//append_imports_end

declare const window: any;
declare const cordova: any;

@Injectable()
export class branchdataservice {
  constructor(
    private sdService: SDBaseService,
    private router: Router,
    private matSnackBar: MatSnackBar
  ) {}

  //   service flows_branchdataservice

  public async getbranchDetails(...others) {
    let bh = { input: {}, local: { result: '' } };
    try {
      bh = this.sdService.__constructDefault(bh);

      bh = await this.sd_fijKT7NRp9noLbX7(bh);
      //appendnew_next_getbranchDetails
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_V9A1eehwhzh3SWqa');
    }
  }
  //appendnew_flow_branchdataservice_Start

  async sd_fijKT7NRp9noLbX7(bh) {
    try {
      let requestOptions = {
        url: 'http://localhost:8081/api/data/getBranches',
        method: 'get',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.body
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_fijKT7NRp9noLbX7
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_fijKT7NRp9noLbX7');
    }
  }
  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (false /*appendnew_next_Catch*/) {
      return bh;
    } else {
      throw e;
    }
  }
  //appendnew_flow_branchdataservice_Catch
}
