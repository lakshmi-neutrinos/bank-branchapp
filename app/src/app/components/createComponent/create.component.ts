/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit , Inject } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ReactiveFormsModule, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-create',
    templateUrl: './create.template.html'
})

export class createComponent extends NBaseComponent implements OnInit {

createForm : FormGroup;
    constructor(public dialogRef: MatDialogRef<createComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private data: any ) {
        super();
    }

    ngOnInit() {
    
    this.createForm = this.fb.group({
            branchCode: [''],
            branchName: [''],
            bankCode: [''],
            branchStatus: [''],
            headOffice: [''],
           
        }); 
    }

    branchstatus = ['Open','Close'];
    headOffice = ['True','False'];

    create(){
        console.log(this.createForm);
        
    }
   cancel(){
       this.dialogRef.close();
   }
}
