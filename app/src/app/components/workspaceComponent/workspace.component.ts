/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit , Inject} from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import{ createComponent } from '../createComponent/create.component';

import {branchdataservice} from '../../sd-services/branchdataservice';
import {MatTableDataSource} from '@angular/material';

/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-workspace',
    templateUrl: './workspace.template.html'
})

export class workspaceComponent extends NBaseComponent implements OnInit {

    branchdata;
    
    constructor(private branchdataservice:branchdataservice, public dialog: MatDialog) {
   
        super();
    }

    ngOnInit() {
        this.getdata();
    }

    getdata()
    {
        this.branchdataservice.getbranchDetails().then((data)=>{
            console.log(data.local.result);
            this.branchdata = new MatTableDataSource(this.leadObjtoArr(data.local.result));
        })
    }

    leadObjtoArr(obj) {
    return Array.from(Object.keys(obj), k => obj[k]);
    }     
    openDialog(): void {
    const dialogRef = this.dialog.open(createComponent, {
      width: '400px',
      //data: {name: this.name, animal: this.animal}
    });
}
}
