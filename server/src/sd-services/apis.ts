import { SDBaseService } from '../services/SDBaseService';
import * as httpStatusCodes from 'http-status-codes';
import { Middleware } from '../middleware/Middleware';
import log from '../utils/Logger';
import { Parsers } from '../helper/parsers/parsers';
import * as cookieParser from 'cookie-parser';
import { Readable } from 'stream';
import { setInterval } from 'timers';
import * as settings from '../config/config';
import { Issuer, custom } from 'openid-client';
import * as crypto from 'crypto';
import * as url from 'url';

let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start

import { MongoPersistance } from '../helper/generic/MongoPersistance'; //_splitter_
import * as mongodb from 'mongodb'; //_splitter_
//append_imports_end

export class apis {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'apis';
    this.app = app;
    this.serviceBasePath = `${this.app.settings.base}`;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new apis(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_apis_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: apis');

    //appendnew_flow_apis_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: apis');

    this.swaggerDocument['paths']['/data/getBranches'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/data/getBranches`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_inrD8v1oxqNvCvuW(bh);
          //appendnew_next_sd_sGuZAQu6eRRgJhem
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_sGuZAQu6eRRgJhem');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );
    //appendnew_flow_apis_HttpIn
  }
  //   service flows_apis

  //appendnew_flow_apis_Start

  async sd_inrD8v1oxqNvCvuW(bh) {
    try {
      bh.response = await MongoPersistance.getInstance().find(
        'sd_OzT1N314ucLHb2dx',
        'branches',
        bh.input.body,
        {}
      );
      bh = await this.sd_47QXzxyaddeAduiH(bh);
      //appendnew_next_sd_inrD8v1oxqNvCvuW
    } catch (e) {
      await this.errorHandler(bh, e, 'sd_inrD8v1oxqNvCvuW');
    }

    return bh;
  }

  async sd_47QXzxyaddeAduiH(bh) {
    try {
      bh.result = [];

      bh.response.filter(ele => {
        let temp = {
          branchcode: ele.branchcode,
          branchname: ele.branchname,
          bankcode: ele.bankcode,
          branchstatus: ele.branchstatus,
          headoffice: ele.headoffice
        };

        bh.result.push(temp);
      });

      await this.sd_1R6GhevMM1nsg5t5(bh);
      //appendnew_next_sd_47QXzxyaddeAduiH
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_47QXzxyaddeAduiH');
    }
  }
  async sd_1R6GhevMM1nsg5t5(bh) {
    try {
      bh.web.res.status(200).send(bh.result);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_1R6GhevMM1nsg5t5');
    }
  }
  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (false /*appendnew_next_Catch*/) {
      return bh;
    } else {
      throw e;
    }
  }
  //appendnew_flow_apis_Catch
}
