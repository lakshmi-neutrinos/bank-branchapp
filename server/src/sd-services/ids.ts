import { SDBaseService } from '../services/SDBaseService';
import * as httpStatusCodes from 'http-status-codes';
import { Middleware } from '../middleware/Middleware';
import log from '../utils/Logger';
import { Parsers } from '../helper/parsers/parsers';
import * as cookieParser from 'cookie-parser';
import { Readable } from 'stream';
import { setInterval } from 'timers';
import * as settings from '../config/config';
import { Issuer, custom } from 'openid-client';
import * as crypto from 'crypto';
import * as url from 'url';

let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start

import { idsutil } from './idsutil'; //_splitter_
//append_imports_end

export class ids {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'ids';
    this.app = app;
    this.serviceBasePath = `${this.app.settings.base}`;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new ids(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_ids_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: ids');

    let mw_hrefstart: Middleware = new Middleware(
      this.serviceName,
      'hrefstart',
      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault({ local: {} }, req, res, next);
          bh = await this.sd_aOjiWPnefXQatHk8(bh);
          //appendnew_next_mw_hrefstart
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_IncXXRqQ5rLNfUaB');
        }
      }
    );
    this.generatedMiddlewares[this.serviceName]['hrefstart'] = mw_hrefstart;

    let mw_Authorize: Middleware = new Middleware(
      this.serviceName,
      'Authorize',
      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault({ local: {} }, req, res, next);
          bh = await this.sd_NAvLlEG6i4YXfUqK(bh);
          //appendnew_next_mw_Authorize
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_NbMzD1ui77GTN15j');
        }
      }
    );
    this.generatedMiddlewares[this.serviceName]['Authorize'] = mw_Authorize;

    //appendnew_flow_ids_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: ids');

    this.swaggerDocument['paths']['/login'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/login`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_qe7Z8AkfM2idksEt(bh);
          //appendnew_next_sd_ToyBmbEvddmoiiN0
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_ToyBmbEvddmoiiN0');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/login/cb'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/login/cb`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_BpTCOlUbqK2HLaBX(bh);
          //appendnew_next_sd_hg0fjXgrwUKCkZVY
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_hg0fjXgrwUKCkZVY');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/user/info'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/user/info`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        'IDSAuthroizedAPIs',
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_e1z61wRd3tipJTQW(bh);
          //appendnew_next_sd_YppRplY14RkYBYsa
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_YppRplY14RkYBYsa');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        'IDSAuthroizedAPIs',
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/logout'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/logout`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_NHDNKeeAB2afgWp4(bh);
          //appendnew_next_sd_7067y7WrENj8S1xp
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_7067y7WrENj8S1xp');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    this.swaggerDocument['paths']['/logout/cb'] = {
      get: {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      }
    };
    this.app['get'](
      `${this.serviceBasePath}/logout/cb`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_V4RxF1Wg7LS0NJiy(bh);
          //appendnew_next_sd_s07nfO4AUsq8P25E
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_s07nfO4AUsq8P25E');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );
    //appendnew_flow_ids_HttpIn
  }
  //   service flows_ids

  //appendnew_flow_ids_Start

  async sd_qe7Z8AkfM2idksEt(bh) {
    try {
      bh.local.idsConfigured = false;
      if (
        settings.default.hasOwnProperty('ids') &&
        settings.default['ids'].hasOwnProperty('client_id') &&
        settings.default['ids'].hasOwnProperty('client_secret')
      ) {
        bh.local.idsConfigured = true;
      }

      bh = await this.sd_rHSGlPSk4XAJSQ2Q(bh);
      //appendnew_next_sd_qe7Z8AkfM2idksEt
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_qe7Z8AkfM2idksEt');
    }
  }

  async sd_rHSGlPSk4XAJSQ2Q(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.idsConfigured,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_JTKhxz3m4G3cPkCN(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_4xot7XABDT7LUBaG(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_rHSGlPSk4XAJSQ2Q');
    }
  }

  async sd_JTKhxz3m4G3cPkCN(bh) {
    try {
      bh.local.reqParams = {
        state: crypto.randomBytes(16).toString('hex'),
        nonce: crypto.randomBytes(16).toString('hex'),
        isMobile: bh.input.query.isMobile,
        redirectTo: bh.input.query.redirectTo
      };

      bh = await this.sd_1sL2KygRsLkLlyMT(bh);
      //appendnew_next_sd_JTKhxz3m4G3cPkCN
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_JTKhxz3m4G3cPkCN');
    }
  }

  async sd_1sL2KygRsLkLlyMT(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.reqParams;
    }
    bh = await this.sd_WHnKQLpsxISSr7uN(bh);
    //appendnew_next_sd_1sL2KygRsLkLlyMT
    return bh;
  }

  async sd_WHnKQLpsxISSr7uN(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_YVOAeQzLeNEoieoc(bh);
      //appendnew_next_sd_WHnKQLpsxISSr7uN
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_WHnKQLpsxISSr7uN');
    }
  }
  async sd_YVOAeQzLeNEoieoc(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getAuthorizationParams();
      bh.input.authParams = outputVariables.input.authParams;

      bh = await this.sd_xL4g0DGDhKysMrsC(bh);
      //appendnew_next_sd_YVOAeQzLeNEoieoc
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_YVOAeQzLeNEoieoc');
    }
  }
  async sd_xL4g0DGDhKysMrsC(bh) {
    try {
      const authorizationRequest = Object.assign(
        {
          redirect_uri: url.resolve(bh.web.req.href, '/api/login/cb'),
          scope: 'openid profile email address phone user',
          state: bh.local.reqParams.state,
          nonce: bh.local.reqParams.nonce,
          response_type: bh.input.client.response_types[0]
        },
        bh.input.authParams
      );

      bh.local.redirectHeaders = {
        location: bh.input.client.authorizationUrl(authorizationRequest)
      };

      await this.sd_NlMVcTYS5JBvYrRC(bh);
      //appendnew_next_sd_xL4g0DGDhKysMrsC
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_xL4g0DGDhKysMrsC');
    }
  }
  async sd_NlMVcTYS5JBvYrRC(bh) {
    bh.web.res.set(bh.local.redirectHeaders);

    try {
      bh.web.res.status(302).send('redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_NlMVcTYS5JBvYrRC');
    }
  }
  async sd_4xot7XABDT7LUBaG(bh) {
    try {
      bh.local.res = {
        message:
          'IDS client not registered. Register on the Neutrinos Stuido and try again'
      };

      await this.sd_D6E2jZznqtsWCAQr(bh);
      //appendnew_next_sd_4xot7XABDT7LUBaG
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_4xot7XABDT7LUBaG');
    }
  }
  async sd_D6E2jZznqtsWCAQr(bh) {
    try {
      bh.web.res.status(404).send(bh.local.res.message);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_D6E2jZznqtsWCAQr');
    }
  }
  async sd_aOjiWPnefXQatHk8(bh) {
    try {
      const protocol =
        bh.input.headers['x-forwarded-proto'] || bh.web.req.protocol;
      const href =
        protocol + '://' + bh.web.req.get('Host') + bh.web.req.originalUrl;
      bh.web.req.href = href;

      await this.sd_t4eUqnnHCofLkoyI(bh);
      //appendnew_next_sd_aOjiWPnefXQatHk8
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_aOjiWPnefXQatHk8');
    }
  }
  async sd_t4eUqnnHCofLkoyI(bh) {
    try {
      bh.web.next();

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_t4eUqnnHCofLkoyI');
    }
  }

  async sd_BpTCOlUbqK2HLaBX(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.input.sessionParams = JSON.parse(
        JSON.stringify(requestObject.session)
      );
    }
    bh = await this.sd_iZHGboHZyC6dT8PM(bh);
    //appendnew_next_sd_BpTCOlUbqK2HLaBX
    return bh;
  }

  async sd_iZHGboHZyC6dT8PM(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_94G1rgMMJkHwUsFI(bh);
      //appendnew_next_sd_iZHGboHZyC6dT8PM
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_iZHGboHZyC6dT8PM');
    }
  }
  async sd_94G1rgMMJkHwUsFI(bh) {
    try {
      const params = bh.input.client.callbackParams(bh.web.req);
      let tokenset = await bh.input.client.callback(
        url.resolve(bh.web.req.href, 'cb'),
        params,
        {
          nonce: bh.input.sessionParams.data.nonce,
          state: bh.input.sessionParams.data.state
        }
      );

      bh.local.redirectTo = bh.input.sessionParams.data.redirectTo;

      bh.local.userDetails = {
        tokenset: Object.assign({}, tokenset),
        userInfo: await bh.input.client.userinfo(tokenset['access_token'])
      };
      bh.local.userDetails['tokenset']['claims'] = Object.assign(
        {},
        tokenset.claims()
      );

      bh = await this.sd_YqEd1yMtJMH1MGw3(bh);
      //appendnew_next_sd_94G1rgMMJkHwUsFI
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_94G1rgMMJkHwUsFI');
    }
  }

  async sd_YqEd1yMtJMH1MGw3(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.userDetails;
    }
    bh = await this.sd_88ddUOV6FaO9Ew5m(bh);
    //appendnew_next_sd_YqEd1yMtJMH1MGw3
    return bh;
  }

  async sd_88ddUOV6FaO9Ew5m(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['se'](
          bh.input.sessionParams.data.isMobile,
          'true',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_DKYViqsP1426uwxx(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_BHCTYXDZ7vrNWtLL(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_88ddUOV6FaO9Ew5m');
    }
  }

  async sd_DKYViqsP1426uwxx(bh) {
    try {
      bh.local.htmlResponse = `
 <html>
   <script>
      let _timer;
      _timer = setInterval(() => {
                  if(window.webkit) {
                      window.webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({'auth': 'success'}));
                      clearInterval(_timer);
                  }
              }, 250);
      
   </script>
</html>`;

      await this.sd_kviY9FUM7x4d2tqI(bh);
      //appendnew_next_sd_DKYViqsP1426uwxx
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_DKYViqsP1426uwxx');
    }
  }
  async sd_kviY9FUM7x4d2tqI(bh) {
    try {
      bh.web.res.status(200).send(bh.local.htmlResponse);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_kviY9FUM7x4d2tqI');
    }
  }
  async sd_BHCTYXDZ7vrNWtLL(bh) {
    try {
      bh.local.redirectHeaders = {
        location: bh.local.redirectTo
      };

      await this.sd_0ZVKrvOr9X8R0piH(bh);
      //appendnew_next_sd_BHCTYXDZ7vrNWtLL
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_BHCTYXDZ7vrNWtLL');
    }
  }
  async sd_0ZVKrvOr9X8R0piH(bh) {
    bh.web.res.set(bh.local.redirectHeaders);

    try {
      bh.web.res.status(302).send('Redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_0ZVKrvOr9X8R0piH');
    }
  }

  async sd_e1z61wRd3tipJTQW(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.session = JSON.parse(JSON.stringify(requestObject.session));
    }
    await this.sd_WOgnkouqgyupqSp1(bh);
    //appendnew_next_sd_e1z61wRd3tipJTQW
    return bh;
  }

  async sd_WOgnkouqgyupqSp1(bh) {
    try {
      bh.web.res.status(200).send(bh.local.session.data.userInfo);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_WOgnkouqgyupqSp1');
    }
  }
  async sd_VFbDlvRTCjHqmvjC(bh) {
    try {
      bh.web.res.redirect('/api/login');

      //appendnew_next_sd_VFbDlvRTCjHqmvjC
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_VFbDlvRTCjHqmvjC');
    }
  }

  async sd_NHDNKeeAB2afgWp4(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.sessionData = JSON.parse(JSON.stringify(requestObject.session));
    }
    bh = await this.sd_nmfNoaNs42O6jus7(bh);
    //appendnew_next_sd_NHDNKeeAB2afgWp4
    return bh;
  }

  async sd_nmfNoaNs42O6jus7(bh) {
    try {
      bh.local.sessionExists = false;
      if (
        bh.local.sessionData &&
        bh.local.sessionData.data &&
        bh.local.sessionData.data.tokenset
      ) {
        bh.local.sessionData['data']['redirectTo'] =
          bh.input.query['redirectTo'];
        bh.local.sessionData['data']['isMobile'] = bh.input.query['isMobile'];
        bh.local.sessionExists = true;
      } else {
        delete bh.local.sessionData['redirectTo'];
      }

      bh = await this.sd_kOqWo0uGOBvRdJFk(bh);
      //appendnew_next_sd_nmfNoaNs42O6jus7
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_nmfNoaNs42O6jus7');
    }
  }

  async sd_kOqWo0uGOBvRdJFk(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.sessionData.data;
    }
    bh = await this.sd_boAqf1ayzYp4FiYB(bh);
    //appendnew_next_sd_kOqWo0uGOBvRdJFk
    return bh;
  }

  async sd_boAqf1ayzYp4FiYB(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_igucS8XxsRRfTXkN(bh);
      //appendnew_next_sd_boAqf1ayzYp4FiYB
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_boAqf1ayzYp4FiYB');
    }
  }

  async sd_igucS8XxsRRfTXkN(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.sessionExists,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_rvTCnWuHekEulquP(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_cTJnNNnMRuNTJflw(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_igucS8XxsRRfTXkN');
    }
  }

  async sd_rvTCnWuHekEulquP(bh) {
    try {
      await Promise.all([
        bh.local.sessionData.data.tokenset.access_token
          ? bh.input.client.revoke(
              bh.local.sessionData.data.tokenset.access_token,
              'access_token'
            )
          : undefined,
        bh.local.sessionData.data.tokenset.refresh_token
          ? bh.input.client.revoke(
              bh.local.sessionData.data.tokenset.refresh_token,
              'refresh_token'
            )
          : undefined
      ]);

      bh.local.res = {
        idsURL: url.format(
          Object.assign(
            url.parse(bh.input.client.issuer.end_session_endpoint),
            {
              search: null,
              query: {
                id_token_hint: bh.local.sessionData.data.tokenset.id_token,
                post_logout_redirect_uri: url.resolve(
                  bh.web.req.href,
                  '/api/logout/cb'
                ),
                client_id: settings.default['ids']['client_id']
              }
            }
          )
        ),
        sessionExists: true
      };

      await this.sd_HeAqwzjSicTLFthi(bh);
      //appendnew_next_sd_rvTCnWuHekEulquP
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_rvTCnWuHekEulquP');
    }
  }
  async sd_HeAqwzjSicTLFthi(bh) {
    try {
      bh.web.res.status(200).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_HeAqwzjSicTLFthi');
    }
  }
  async sd_cTJnNNnMRuNTJflw(bh) {
    try {
      bh.local.res = {
        sessionExists: false
      };

      await this.sd_HeAqwzjSicTLFthi(bh);
      //appendnew_next_sd_cTJnNNnMRuNTJflw
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_cTJnNNnMRuNTJflw');
    }
  }

  async sd_V4RxF1Wg7LS0NJiy(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.sessionData = JSON.parse(JSON.stringify(requestObject.session));
    }
    bh = await this.sd_GcoA8EqgduOQZjmn(bh);
    //appendnew_next_sd_V4RxF1Wg7LS0NJiy
    return bh;
  }

  async sd_GcoA8EqgduOQZjmn(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      let p = function() {
        return new Promise((resolve, reject) => {
          requestObject.session.destroy(function(error) {
            if (error) {
              return reject(error);
            }
            return resolve();
          });
        });
      };
      try {
        await p();
        bh = await this.sd_Ln7U7bRKdIkQ8IXb(bh);
        //appendnew_next_sd_GcoA8EqgduOQZjmn
      } catch (e) {
        return await this.errorHandler(bh, e, 'sd_GcoA8EqgduOQZjmn');
      }
      return bh;
    }
  }

  async sd_Ln7U7bRKdIkQ8IXb(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['se'](
          bh.local.sessionData.data.isMobile,
          'true',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_2ttMg1mmsZg0US8g(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_viVkvgAgxHuQTaI3(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Ln7U7bRKdIkQ8IXb');
    }
  }

  async sd_2ttMg1mmsZg0US8g(bh) {
    try {
      bh.local.res = `<html>
   <script>
      var _timer;
      _timer = setInterval(() => {
                  if(window.webkit) {
                      window.webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({'auth': 'success'}));
                      clearInterval(_timer);
                  }
              }, 250);
      
   </script>
</html>`;

      await this.sd_p5k4sMXwLqiKi7oR(bh);
      //appendnew_next_sd_2ttMg1mmsZg0US8g
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_2ttMg1mmsZg0US8g');
    }
  }
  async sd_p5k4sMXwLqiKi7oR(bh) {
    try {
      bh.web.res.status(200).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_p5k4sMXwLqiKi7oR');
    }
  }
  async sd_viVkvgAgxHuQTaI3(bh) {
    try {
      bh.local.redirectHeaders = {
        location: bh.local.sessionData.data.redirectTo
      };

      await this.sd_e3EUDfsYN0vi0yLA(bh);
      //appendnew_next_sd_viVkvgAgxHuQTaI3
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_viVkvgAgxHuQTaI3');
    }
  }
  async sd_e3EUDfsYN0vi0yLA(bh) {
    bh.web.res.set(bh.local.redirectHeaders);

    try {
      bh.web.res.status(302).send('redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_e3EUDfsYN0vi0yLA');
    }
  }
  async sd_NAvLlEG6i4YXfUqK(bh) {
    try {
      bh.local = {};

      bh = await this.sd_lvhh3u8ZVXrq3Vqm(bh);
      //appendnew_next_sd_NAvLlEG6i4YXfUqK
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_NAvLlEG6i4YXfUqK');
    }
  }

  async sd_lvhh3u8ZVXrq3Vqm(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      bh.local.sessionData = JSON.parse(JSON.stringify(requestObject.session));
    }
    bh = await this.sd_x6G4oopAv48BMOzw(bh);
    //appendnew_next_sd_lvhh3u8ZVXrq3Vqm
    return bh;
  }

  async sd_x6G4oopAv48BMOzw(bh) {
    try {
      bh.local.sessionExists = false;

      if (
        bh.local.sessionData &&
        bh.local.sessionData.data &&
        bh.local.sessionData.data.tokenset &&
        bh.local.sessionData.data.tokenset.access_token &&
        bh.local.sessionData.data.tokenset.refresh_token
      ) {
        bh.local.sessionExists = true;
      }

      bh = await this.sd_DOw0NDCNgJEOHezz(bh);
      //appendnew_next_sd_x6G4oopAv48BMOzw
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_x6G4oopAv48BMOzw');
    }
  }

  async sd_DOw0NDCNgJEOHezz(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.sessionExists,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_ve4gGHJOuNp8y2oz(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_po4mcFg1bI52Si1y(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_DOw0NDCNgJEOHezz');
    }
  }

  async sd_ve4gGHJOuNp8y2oz(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.handleTokenExpiry(
        bh.local.sessionData,
        null
      );
      bh.local.newSession = outputVariables.input.newSession;

      bh = await this.sd_DTZoHM4GoroGToaL(bh);
      //appendnew_next_sd_ve4gGHJOuNp8y2oz
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_ve4gGHJOuNp8y2oz');
    }
  }

  async sd_DTZoHM4GoroGToaL(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['false'](
          bh.local.newSession,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_tjxJSq43rU0i7giy(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_XSmvFU8lNbEctp5z(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_DTZoHM4GoroGToaL');
    }
  }

  async sd_tjxJSq43rU0i7giy(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      let p = function() {
        return new Promise((resolve, reject) => {
          requestObject.session.destroy(function(error) {
            if (error) {
              return reject(error);
            }
            return resolve();
          });
        });
      };
      try {
        await p();
        bh = await this.sd_zQLSxG8Cpm495xbl(bh);
        //appendnew_next_sd_tjxJSq43rU0i7giy
      } catch (e) {
        return await this.errorHandler(bh, e, 'sd_tjxJSq43rU0i7giy');
      }
      return bh;
    }
  }

  async sd_zQLSxG8Cpm495xbl(bh) {
    try {
      bh.local.res = {
        code: 'TOKEN_EXPIRED',
        message: 'Token invalid or access revoked'
      };

      await this.sd_Pnr1tx0eKb5J8COS(bh);
      //appendnew_next_sd_zQLSxG8Cpm495xbl
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_zQLSxG8Cpm495xbl');
    }
  }
  async sd_Pnr1tx0eKb5J8COS(bh) {
    try {
      bh.web.res.status(403).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Pnr1tx0eKb5J8COS');
    }
  }

  async sd_XSmvFU8lNbEctp5z(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.newSession.rotated,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_oZMTIk5uUf3r2Ie6(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_rKym0c3XHlDISrtQ(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_XSmvFU8lNbEctp5z');
    }
  }

  async sd_oZMTIk5uUf3r2Ie6(bh) {
    try {
      delete bh.local.newSession.rotated;

      bh = await this.sd_Eny3Z6f8pnAol7rk(bh);
      //appendnew_next_sd_oZMTIk5uUf3r2Ie6
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_oZMTIk5uUf3r2Ie6');
    }
  }

  async sd_Eny3Z6f8pnAol7rk(bh) {
    let requestObject = bh.web.req;
    if (requestObject.session) {
      requestObject.session.data = bh.local.newSession;
    }
    await this.sd_rKym0c3XHlDISrtQ(bh);
    //appendnew_next_sd_Eny3Z6f8pnAol7rk
    return bh;
  }

  async sd_rKym0c3XHlDISrtQ(bh) {
    try {
      bh.web.next();

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_rKym0c3XHlDISrtQ');
    }
  }

  async sd_po4mcFg1bI52Si1y(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['cont'](
          bh.input.path,
          '/user/info',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_IWJUZLKRwBKGEp8L(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_TRo2MWbowfJd6jbK(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_po4mcFg1bI52Si1y');
    }
  }

  async sd_IWJUZLKRwBKGEp8L(bh) {
    try {
      bh.local.res = { message: 'Session expired' };

      await this.sd_Pnr1tx0eKb5J8COS(bh);
      //appendnew_next_sd_IWJUZLKRwBKGEp8L
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_IWJUZLKRwBKGEp8L');
    }
  }
  async sd_TRo2MWbowfJd6jbK(bh) {
    try {
      bh.local.res = { code: 'NO_SESSION', message: 'Session not present' };

      await this.sd_Pnr1tx0eKb5J8COS(bh);
      //appendnew_next_sd_TRo2MWbowfJd6jbK
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_TRo2MWbowfJd6jbK');
    }
  }
  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false ||
      (await this.sd_mXvLbAHC8U0LiHt0(bh)) ||
      (await this.sd_JxAyNs0kxX0sAs23(bh)) /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      throw e;
    }
  }

  async sd_mXvLbAHC8U0LiHt0(bh) {
    const nodes = [
      'sd_YVOAeQzLeNEoieoc',
      'sd_hg0fjXgrwUKCkZVY',
      'sd_iZHGboHZyC6dT8PM',
      'sd_94G1rgMMJkHwUsFI',
      'sd_BpTCOlUbqK2HLaBX',
      'sd_88ddUOV6FaO9Ew5m',
      'sd_DKYViqsP1426uwxx',
      'sd_BHCTYXDZ7vrNWtLL',
      'sd_kviY9FUM7x4d2tqI',
      'sd_0ZVKrvOr9X8R0piH'
    ];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_VFbDlvRTCjHqmvjC(bh);
      //appendnew_next_sd_mXvLbAHC8U0LiHt0
      return true;
    }
    return false;
  }

  async sd_JxAyNs0kxX0sAs23(bh) {
    const nodes = ['sd_ve4gGHJOuNp8y2oz'];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_zQLSxG8Cpm495xbl(bh);
      //appendnew_next_sd_JxAyNs0kxX0sAs23
      return true;
    }
    return false;
  }

  //appendnew_flow_ids_Catch
}
