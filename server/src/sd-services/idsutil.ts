import { SDBaseService } from '../services/SDBaseService';
import * as httpStatusCodes from 'http-status-codes';
import { Middleware } from '../middleware/Middleware';
import log from '../utils/Logger';
import { Parsers } from '../helper/parsers/parsers';
import * as cookieParser from 'cookie-parser';
import { Readable } from 'stream';
import { setInterval } from 'timers';
import * as settings from '../config/config';
import { Issuer, custom } from 'openid-client';
import * as crypto from 'crypto';
import * as url from 'url';

let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start
//append_imports_end
export class idsutil {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'idsutil';
    this.app = app;
    this.serviceBasePath = `${this.app.settings.base}`;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new idsutil(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_idsutil_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: idsutil');

    //appendnew_flow_idsutil_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: idsutil');
    //appendnew_flow_idsutil_HttpIn
  }
  //   service flows_idsutil

  public async getIDSClientInstance(clientInstance = null, ...others) {
    let bh = { input: { clientInstance: clientInstance }, local: {} };
    try {
      bh = this.sdService.__constructDefault(bh);

      bh = await this.sd_zpyZoPI4PRrClJXh(bh);
      //appendnew_next_getIDSClientInstance
      //Start formatting output variables
      let outputVariables = {
        input: { clientInstance: bh.input.clientInstance },
        local: {}
      };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_GjlrnERT2BskgXmj');
    }
  }
  public async getAuthorizationParams(authParams = null, ...others) {
    let bh = { input: { authParams: authParams }, local: {} };
    try {
      bh = this.sdService.__constructDefault(bh);

      bh = await this.sd_LXlaSk3A04mPWGnK(bh);
      //appendnew_next_getAuthorizationParams
      //Start formatting output variables
      let outputVariables = {
        input: { authParams: bh.input.authParams },
        local: {}
      };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_BUjLkQZIEQo1jWWE');
    }
  }
  public async handleTokenExpiry(
    existingSession = '',
    newSession = '',
    ...others
  ) {
    let bh = {
      input: { existingSession: existingSession, newSession: newSession },
      local: {}
    };
    try {
      bh = this.sdService.__constructDefault(bh);

      bh = await this.sd_auQav8EX3urplPNc(bh);
      //appendnew_next_handleTokenExpiry
      //Start formatting output variables
      let outputVariables = {
        input: { newSession: bh.input.newSession },
        local: {}
      };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_TqnIoJt3bBzUKCv8');
    }
  }
  //appendnew_flow_idsutil_Start

  //new_service_variable_client
  client: any;
  async sd_zpyZoPI4PRrClJXh(bh) {
    try {
      bh.local.client = this['client'];
      bh = await this.sd_XhBQ8ElFmU4zYxzY(bh);
      //appendnew_next_sd_zpyZoPI4PRrClJXh
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_zpyZoPI4PRrClJXh');
    }
  }

  async sd_XhBQ8ElFmU4zYxzY(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['istype'](
          bh.local.client,
          'undefined',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_XK4CRTvg4V7PgaLg(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_LTESfLWE8gjZq7Rr(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_XhBQ8ElFmU4zYxzY');
    }
  }

  async sd_XK4CRTvg4V7PgaLg(bh) {
    try {
      const DEFAULT_HTTP_OPTIONS = {
        timeout: 60000
      };

      custom.setHttpOptionsDefaults({
        timeout: DEFAULT_HTTP_OPTIONS.timeout
      });
      log.info(
        `Identity server default HTTP options : ${DEFAULT_HTTP_OPTIONS}`
      );
      const issuer = await Issuer.discover(
        settings.default['ids']['issuerURL']
      );
      log.info(`Identity server discovered at : ${issuer.issuer}`);
      const client = await new issuer.Client({
        client_id: settings.default['ids']['client_id'],
        client_secret: settings.default['ids']['client_secret']
      });
      client[custom.clock_tolerance] = process.env.CLOCK_TOLERANCE;
      log.info('Client connected...');
      bh.input.clientInstance = client;

      bh = await this.sd_JieQYA5iu5Q0XBnc(bh);
      //appendnew_next_sd_XK4CRTvg4V7PgaLg
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_XK4CRTvg4V7PgaLg');
    }
  }

  async sd_JieQYA5iu5Q0XBnc(bh) {
    try {
      this['client'] = bh.input.clientInstance;
      //appendnew_next_sd_JieQYA5iu5Q0XBnc
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_JieQYA5iu5Q0XBnc');
    }
  }

  async sd_LTESfLWE8gjZq7Rr(bh) {
    try {
      bh.input.clientInstance = this['client'];
      //appendnew_next_sd_LTESfLWE8gjZq7Rr
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_LTESfLWE8gjZq7Rr');
    }
  }
  async sd_LXlaSk3A04mPWGnK(bh) {
    try {
      bh.input.authParams = {
        scope: 'openid profile email address phone offline_access user',
        prompt: 'consent'
      };

      //appendnew_next_sd_LXlaSk3A04mPWGnK
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_LXlaSk3A04mPWGnK');
    }
  }
  async sd_auQav8EX3urplPNc(bh) {
    try {
      const tokenset = bh.input.existingSession.data.tokenset;
      bh.local.expires_at = tokenset['expires_at'] * 1000;
      bh.local.refreshTime = new Date().valueOf() + 300000; // 5min before

      bh = await this.sd_4jvzk5G9rpeciEHG(bh);
      //appendnew_next_sd_auQav8EX3urplPNc
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_auQav8EX3urplPNc');
    }
  }

  async sd_4jvzk5G9rpeciEHG(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['gt'](
          bh.local.expires_at,
          bh.local.refreshTime,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_lzJlMuy9G69NT2ju(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_vMo9121DqdPiZjK9(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_4jvzk5G9rpeciEHG');
    }
  }

  async sd_lzJlMuy9G69NT2ju(bh) {
    try {
      bh.input.newSession = bh.input.existingSession.data;

      //appendnew_next_sd_lzJlMuy9G69NT2ju
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_lzJlMuy9G69NT2ju');
    }
  }
  async sd_vMo9121DqdPiZjK9(bh) {
    try {
      const idsutilInstance = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_nWoVEIeyhauB3vbt(bh);
      //appendnew_next_sd_vMo9121DqdPiZjK9
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_vMo9121DqdPiZjK9');
    }
  }
  async sd_nWoVEIeyhauB3vbt(bh) {
    try {
      bh.local.refresh_token = await bh.input.client.introspect(
        bh.input.existingSession.data.tokenset.refresh_token,
        'refresh_token'
      );

      bh = await this.sd_tIiSzaTQjRq5zZ50(bh);
      //appendnew_next_sd_nWoVEIeyhauB3vbt
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_nWoVEIeyhauB3vbt');
    }
  }

  async sd_tIiSzaTQjRq5zZ50(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.refresh_token.active,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_FgbCozcIsZTsH3oB(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_lBijY4y8jTxT55xR(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_tIiSzaTQjRq5zZ50');
    }
  }

  async sd_FgbCozcIsZTsH3oB(bh) {
    try {
      bh.input.newSession = { rotated: true };
      bh.input.newSession['tokenset'] = await bh.input.client.refresh(
        bh.input.existingSession.data.tokenset.refresh_token
      );
      bh.input.newSession['userInfo'] = await bh.input.client.userinfo(
        bh.input.newSession['tokenset']['access_token']
      );
      bh.input.newSession['tokenset']['claims'] = Object.assign(
        {},
        bh.input.newSession['tokenset'].claims()
      );

      //appendnew_next_sd_FgbCozcIsZTsH3oB
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_FgbCozcIsZTsH3oB');
    }
  }
  async sd_lBijY4y8jTxT55xR(bh) {
    try {
      bh.input.newSession = false;

      //appendnew_next_sd_lBijY4y8jTxT55xR
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_lBijY4y8jTxT55xR');
    }
  }
  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false ||
      (await this.sd_8pzqMWF6G2U7cvlq(bh)) /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      throw e;
    }
  }

  async sd_8pzqMWF6G2U7cvlq(bh) {
    const nodes = ['handleTokenExpiry'];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_lBijY4y8jTxT55xR(bh);
      //appendnew_next_sd_8pzqMWF6G2U7cvlq
      return true;
    }
    return false;
  }

  //appendnew_flow_idsutil_Catch
}
